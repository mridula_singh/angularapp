import { Component, OnInit, Output, ElementRef, ViewChild, EventEmitter } from '@angular/core';
import { MatFormField } from '@angular/material/form-field';
import { MatChip, MatChipRemove } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { FormControl } from '@angular/forms';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  searchInput = new FormControl();
  searchOption = [];
  autoCompleteList = [];
  @ViewChild('autocompleteInput') autocompleteInput: ElementRef;
  @Output() onSelectedOption = new EventEmitter();
  constructor(private productService: ProductService) {
    this.searchInput.valueChanges.subscribe(userInput => {
      if (typeof userInput === 'string' && userInput.length >= 2) {
        this.getAutoCompleteList(userInput);
      } else {
        this.autoCompleteList = [];
      }
    })
  }

  ngOnInit(): void { }

  getAutoCompleteList(userInput): any {
    this.productService.searchProduct(userInput).subscribe((result) => {
      this.autoCompleteList = result;
    })
  }


}
